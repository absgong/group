import { DataGrid, GridRowsProp, GridColDef } from '@material-ui/data-grid';

const rows = [
    { id: 1, resnumber: 558899, contactname: 'Kevin' },
    { id: 2, resnumber: 558877, contactname: 'is Awesome' },
    { id: 3, resnumber: 558866, contactname: 'is Amazing' },
];

const columns = [
    { field: 'resnumber', headerName: 'Res#', width: 150 },
    { field: 'contactname', headerName: 'Contact', width: 150 },
];

function Group() {
    
    return (
        <div style={{ height: 300, width: '100%' }}>
            <DataGrid rows={rows} columns={columns} />
        </div>
    )
}

export default Group;