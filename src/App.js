import './App.css';

import PersistentDrawerLeft from './components/PersistentDrawerLeft'

import {
  BrowserRouter as Router
} from "react-router-dom";

function App() {
  return (
    <Router>
      <PersistentDrawerLeft></PersistentDrawerLeft>
    </Router>
  );
}


export default App;
